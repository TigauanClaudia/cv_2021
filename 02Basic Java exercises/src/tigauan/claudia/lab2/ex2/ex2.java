package tigauan.claudia.lab2.ex2;

public class ex2 {
    public static void main(String[] args) {
        int number = 7;

        ifMethod(number);
        switchMethod(number);

    }

    private static void ifMethod(int number) {
        if (number == 1)  System.out.println("ONE");
        else if(number == 2)  System.out.println("TWO");
        else if(number == 3)  System.out.println("THREE");
        else if(number == 4)  System.out.println("FOUR");
        else if(number == 5)  System.out.println("FIVE");
        else if(number == 6)  System.out.println("SIX");
        else if(number == 7)  System.out.println("SEVEN");
        else if(number == 8)  System.out.println("EIGHT");
        else if(number == 9)  System.out.println("NINE");
        else System.out.println("OTHER");
    }

    private static void switchMethod(int number) {
        String numberString;
        switch (number) {
            case 1:  numberString = "ONE";
                break;
            case 2:  numberString = "TWO";
                break;
            case 3:  numberString = "THREE";
                break;
            case 4:  numberString = "FOUR";
                break;
            case 5:  numberString = "FIVE";
                break;
            case 6:  numberString = "SIX";
                break;
            case 7:  numberString = "SEVEN";
                break;
            case 8:  numberString = "EIGHT";
                break;
            case 9:  numberString = "NINE";
                break;
            default: numberString = "OTHER";
                break;
        }
        System.out.println(numberString);
    }
}
