package tigauan.claudia.lab2.ex3;

import java.util.Scanner;

public class ex3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        int nr=0;
        for(int i=A;i<=B;i++) {
            int g = 0;
            for (int d = 2; d * d <= i; d++)
                if (i % d == 0) g = 1;
            if (g == 0) {
                System.out.println(i + " ");
                nr++;
            }
        }
        System.out.println("\nsunt "+nr+" numere prime");
    }
}
