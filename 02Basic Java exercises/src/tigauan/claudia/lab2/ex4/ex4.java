package tigauan.claudia.lab2.ex4;

import java.util.Scanner;

public class ex4 {
    public static void main(String[] args) {
        int[] v = new int[10];
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int max=0;
        for(int i=0;i<n;i++) {
            v[i] = in.nextInt();
            if (v[i] > max) max = v[i];
        }
        System.out.println("elementul maxim este "+max);
    }
}
