package tigauan.claudia.lab2.ex5;

import java.util.*;

public class ex5 {
    public static void main(String[] args){
        Random r = new Random();

        int[] a = new int[10];

        for(int i=0;i<a.length;i++){
            a[i] = r.nextInt(100);
        }
        System.out.print("inainte de sortare: ");
        for(int i=0;i<a.length;i++){
            System.out.print("a["+i+"]="+a[i]+" ");
        }
        bubbleSort(a);
        System.out.print("\ndupa sortare: ");
        for(int i=0;i<a.length;i++){
            System.out.print("a["+i+"]="+a[i]+" ");
        }

    }
    private static void bubbleSort(int a[])
    {
        int n = a.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (a[j] > a[j+1])
                {
                    // swap arr[j+1] and arr[j]
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
    }
}
