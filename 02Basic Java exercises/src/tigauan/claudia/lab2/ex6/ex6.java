package tigauan.claudia.lab2.ex6;

import java.util.Scanner;

public class ex6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        nonrecursive(n);
        System.out.println(recursive(n));
    }
    private static void nonrecursive(int n)
    {
        int factorial=1;
        for(int i=1;i<=n;i++)
            factorial*=i;
        System.out.println(factorial);

    }
    private static int recursive(int n)
    {
        if(n > 1)
            return n * recursive(n - 1);
        else
            return 1;
    }
}
