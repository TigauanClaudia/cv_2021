package tigauan.claudia.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class ex7 {
    public static void main(String[] args){
        Random r = new Random();
        Scanner in = new Scanner(System.in);
        int n = r.nextInt(10);
        int g=0;
        for(int i=1;i<=3 && g==0;i++)
        {
            System.out.println("number=");
            int a=in.nextInt();
            if(a>n) System.out.println("Wrong answer, your number it too high");
            else if(a<n) System.out.println("Wrong answer, your number it too low");
            else{
                System.out.println("Correct answer");
                g=1;
            }
        }
        if(g==0) System.out.println("You lost");
    }
}
