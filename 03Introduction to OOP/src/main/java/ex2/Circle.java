package ex2;

public class Circle {
    private double radius=1;
    private String color="red";

    Circle()
    {
        System.out.println("Constructor1 has been created!");
    }
    Circle(double radius)
    {
        System.out.println("Constructor2 has been created!");
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
}
