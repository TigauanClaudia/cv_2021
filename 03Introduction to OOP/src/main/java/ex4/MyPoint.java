package ex4;

public class MyPoint {
    int x;
    int y;

    public MyPoint() {
        x = 0;
        y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y)
    {
        this.x=x;
        this.y=y;
    }
    public String toString()
    {
        return "("+x+", "+y+')';
    }
    public double distance(int x, int y)
    {
        double d;
        d=Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y));
        return d;
    }
    public double distance(MyPoint newpoint)
    {
        double d;
        d=Math.sqrt((x-newpoint.x)*(x-newpoint.x)+(y-newpoint.y)*(y-newpoint.y));
        return d;
    }
}
