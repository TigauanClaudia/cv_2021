package ex5;

public class Flower {
    static int nr=0;
    Flower(){
        System.out.println("Flower has been created!");
        nr++;
    }
    public static int number()
    {
        return nr;
    }
    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println(number());
    }


}
