package ex1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRobot {
    @Test
    void testConstructor()
    {
        Robot robot=new Robot();
        assertEquals(1, robot.getx());
    }
    void testChange()
    {
        Robot robot=new Robot();
        assertEquals(1, robot.getx());
        robot.change(5);
        assertEquals(6, robot.getx());
    }

}

