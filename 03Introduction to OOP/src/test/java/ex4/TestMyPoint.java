package ex4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMyPoint {
    @Test
    void testConstructor() {
        MyPoint point=new MyPoint();
        MyPoint point1=new MyPoint(2,3);
        assertEquals(0,point.getX());
        assertEquals(2,point1.getX());
        assertEquals(0,point.getY());
        assertEquals(3,point1.getY());
        point.setX(7);
        point.setY(4);
        System.out.println(point.toString());
        point.setXY(0,0);
        System.out.println(point.toString());
        System.out.println(point.distance(2,2));
        System.out.println(point.distance(point1));
    }
}
