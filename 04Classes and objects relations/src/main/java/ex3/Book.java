package ex3;

import ex2.Author;

public class Book {
    private String name;
    private Author author;
    double price;
    int qtyInStock;

    public Book(String name, Author author, double price) {
        this.author=author;
    }
    public Book(String name, Author author, double price, int qtyInStock) {
        this.author=author;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }
    public String toString()
    {
        return name+" by"+author.toString();
    }
}
