package ex5;

import ex1.Circle;

public class Cylinder extends Circle {
    double height = 1;

    public Cylinder() {

    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return 3.14 * super.getRadius() * super.getRadius() * height;
    }
}
