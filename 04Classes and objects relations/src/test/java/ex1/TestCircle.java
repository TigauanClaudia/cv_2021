package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    void testConstructor()
    {
        Circle circle1=new Circle();
        Circle circle2=new Circle(1);
    }

    void testRadius()
    {
        Circle circle=new Circle();
        assertEquals(1, circle.getRadius());
    }

    void testArea()
    {
        Circle circle=new Circle();
        assertEquals(3.14, circle.getArea());
    }
}
