package ex2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAuthor {
    @Test
    void testConstructor()
    {
        Author author=new Author("John Grisham","john.grisham@gmail.com",'m');
        assertEquals("John Grisham", author.getName());
        assertEquals("john.grisham@gmail.com", author.getEmail());
        assertEquals('m', author.getGender());
        author.setEmail();
        System.out.println(author.getEmail());
        System.out.println(author.toString());

    }
}
