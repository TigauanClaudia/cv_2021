package ex4;

import ex2.Author;
import ex4.Book;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBook {
    @Test
    void testConstructor() {
        Author[] author = new Author[2];
        author[0] = new Author("J.K Rowling", "rowling@gmail.com", 'f');
        author[1] = new Author("Rick Riordan", "riordan@gmail.com", 'm');
        Author author1 = new Author("J.K Rowling", "rowling@gmail.com", 'f');
        Book book = new Book("Harry Potter", author, 45);
        assertEquals(45, book.getPrice());
    }
}
