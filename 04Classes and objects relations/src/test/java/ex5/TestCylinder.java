package ex5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCylinder {
    @Test
    void testConstructor() {
        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(1);
        Cylinder cylinder3 = new Cylinder(1, 3);
        assertEquals(3, cylinder3.getHeight());
        System.out.println(cylinder3.getVolume());
    }
}
