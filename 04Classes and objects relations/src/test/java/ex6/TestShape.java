package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestShape {
    @Test
    void testConstructor() {
        Shape shape1=new Shape();
        Shape shape2=new Shape("blue", false);
        assertEquals("blue", shape2.getColor());
        assertEquals(true, shape1.filled);
        shape1.setColor("purple");
        System.out.println(shape1.getColor());
        shape1.setFilled(true);
        System.out.println(shape1.toString());
        System.out.println(shape2.toString());


    }
}
