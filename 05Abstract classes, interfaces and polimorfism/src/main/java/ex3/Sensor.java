package ex3;

abstract class Sensor {
    String location;
    abstract int readValue();

    public String getLocation() {
        return location;
    }
}
