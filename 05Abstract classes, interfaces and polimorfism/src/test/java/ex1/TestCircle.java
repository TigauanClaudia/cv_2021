package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {

    @Test
    void testConstructor() {
        Circle circle1=new Circle();
        Circle circle2=new Circle(2);
        Circle circle3=new Circle(3, "red",true);
        assertEquals(2,circle2.getRadius());
        System.out.println(circle3.getArea());
        System.out.println(circle2.getPerimeter());
        System.out.println(circle3.toString());

    }
}
