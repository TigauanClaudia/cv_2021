package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRectangle {
    @Test
    void testConstructor() {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2, 3);
        Rectangle rectangle3 = new Rectangle(1, 4, "red", false);
        assertEquals(2, rectangle2.getWidth());
        assertEquals(3, rectangle2.getLength());
        System.out.println(rectangle3.getArea());
        System.out.println(rectangle3.getPerimeter());
        System.out.println(rectangle3.toString());
    }
}
