package ex1;

import com.sun.source.util.SourcePositions;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount>{
    String owner;
    double balance;
    void withdraw(double amount)
    {
        if(balance>amount)  balance=balance-amount;
        else System.out.println(("insufficient founds"));

    }
    void deposit(double amount)
    {
        balance=balance+amount;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public BankAccount() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner : '" + owner + '\'' +
                ", balance : " + balance +
                '}';
    }

    @Override
    public int compareTo(BankAccount o) {
        return Double.compare(this.balance, o.balance);
    }
}
