package ex1;

public class Main {
    public static void main(String[] args) {
        BankAccount b1=new BankAccount("Claudia", 100000);
        BankAccount b2=new BankAccount("Claudia",8000000) ;
        BankAccount b3=new BankAccount("Alex",100000);
        BankAccount b4=new BankAccount("Alex", 100000);

        System.out.println((b1.equals(b2)));
        System.out.println((b1.equals(b3)));
        System.out.println((b3.equals(b4)));
        System.out.println((b2.equals(b4)));
    }
}
