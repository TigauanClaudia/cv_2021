package ex4;

import java.util.Objects;

public class Definition {
    String description;

    public String getDescription() {
        return description;
    }

    public Definition(String description) {
        this.description = description;
    }

    public Definition() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Definition that = (Definition) o;
        return Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
