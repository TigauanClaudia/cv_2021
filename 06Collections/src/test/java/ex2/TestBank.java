package ex2;

import org.junit.jupiter.api.Test;
import ex1.BankAccount;
import ex2.Bank;

import java.util.Comparator;
import java.util.List;

public class TestBank {
    @Test
    void test1(){
        Bank b=new Bank();
        b.addAccount("Claudia1",10000);
        b.addAccount("Claudia2",20000);
        b.addAccount("Claudia3",30000);
        b.addAccount("Claudia4",40000);
        b.addAccount("Claudia5",50000);
        b.printAccounts(20000, 30000);
        List<BankAccount> bankAccounts=b.getBankAccounts();

        bankAccounts.sort(Comparator.comparing(BankAccount::getOwner));
        bankAccounts.forEach(System.out::println);
        System.out.println('\n');
    }
    @Test
    void test2(){
        Bank b=new Bank();
        b.addAccount("Claudia1",10000);
        b.addAccount("Claudia2",20000);
        b.addAccount("Claudia3",30000);
        b.addAccount("Claudia4",40000);
        b.addAccount("Claudia5",50000);
        b.printAccounts(20000, 30000);
        System.out.println('\n');
        b.printAccounts();
    }

}
