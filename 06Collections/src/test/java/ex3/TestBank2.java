package ex3;

import ex1.BankAccount;
//import ex2.Bank;
import ex3.Bank2;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;

public class TestBank2 {
    @Test
    void test1(){
        Bank2 b=new Bank2();
        b.addAccount("Claudia1",10000);
        b.addAccount("Claudia2",20000);
        b.addAccount("Claudia3",30000);
        b.addAccount("Claudia4",40000);
        b.addAccount("Claudia5",50000);
        b.printAccounts(20000, 30000);

        System.out.println('\n');
        b.printAccounts();
        System.out.println('\n');
    }
    @Test
    void test2(){
        Bank2 b=new Bank2();
        b.addAccount("Claudia1",10000);
        b.addAccount("Claudia2",20000);
        b.addAccount("Claudia3",30000);
        b.addAccount("Claudia4",40000);
        b.addAccount("Claudia5",50000);
        b.printAccounts(20000, 30000);
        b.printAccounts();
    }
}
