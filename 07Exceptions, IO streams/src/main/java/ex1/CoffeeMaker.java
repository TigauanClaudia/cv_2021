package ex1;

public class CoffeeMaker {
    private final int nrcoffees;

    public CoffeeMaker(int nrcoffees) {
        this.nrcoffees = nrcoffees;
    }

    Coffee makeCofee() throws Exception {
        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Coffee cofee = new Coffee(t, c);
        if (Coffee.getNrInstances() == nrcoffees) {
            throw new Exception("too many coffees");
        }
        return cofee;
    }
}
