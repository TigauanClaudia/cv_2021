package ex2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;


public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        int count = 0;

        try (BufferedReader in = new BufferedReader(new FileReader("src/main/resources/data.txt"))) {
            int c;
            while ((c = in.read()) != -1) {
                char ch = (char) c;
                if (ch == 'e') count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Letter e " + " " + count);
    }
}
