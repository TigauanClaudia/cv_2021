package ex3;

import java.io.*;

public class EncryptFile {

    public EncryptFile() {
    }

    public static void encrypt(final String path) throws IOException {
        String encryptfile = path.substring(0, path.lastIndexOf('.')) + ".enc";
        try (final FileReader inputFileReader = new FileReader(path);
             final FileWriter outputFileWriter = new FileWriter(encryptfile)) {
            int s;

            while ((s = inputFileReader.read()) != -1) {
                outputFileWriter.write(s - 1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void decrypt(final String path) throws IOException {
        String decryptfile = path.substring(0, path.lastIndexOf('.')) + ".dec";
        try (final FileReader inputFileReader = new FileReader(path);
             final FileWriter outputFileWriter = new FileWriter(decryptfile)) {
            int s;

            while ((s = inputFileReader.read()) != -1) {
                outputFileWriter.write(s + 1);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
