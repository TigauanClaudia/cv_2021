package ex3;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        try {
            EncryptFile.encrypt("src/main/resources/doc.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            EncryptFile.decrypt("src/main/resources/doc.enc");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
