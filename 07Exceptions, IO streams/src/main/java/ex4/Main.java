package ex4;

import java.io.*;
import java.util.Objects;

public class Main {

    private String file = "src/main/resources/cars.txt";


    public static void main(String[] args) {
        Main main = new Main();
        Car car = new Car("Porsche", 250000);
        main.serializeCar(car);
        main.deserializeCar();

    }

    private void serializeCar(Car car) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(car);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Car deserializeCar() {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            Car car = (Car) in.readObject();
            System.out.println(car);
            return car;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
