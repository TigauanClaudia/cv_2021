package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ex2 {
    private JButton button1;
    private JTextField textField1;
    private JPanel panel;

    int nr=0;

    public ex2() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                nr++;
                textField1.setText(nr+" ");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex2");
        frame.setContentPane(new ex2().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
