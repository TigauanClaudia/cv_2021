package ex2;

import javax.swing.*;

public class ex22 {
    private JButton button1;
    private JPanel panel;

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex22");
        frame.setContentPane(new ex22().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
