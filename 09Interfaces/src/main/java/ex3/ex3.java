package ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ex3 {
    private JPanel Panel;
    private JTextField textField1;
    private JButton button1;
    private JTextArea textArea1;

    public ex3() {

        button1.addActionListener(new ActionListener() {
            String text;
            @Override
            public void actionPerformed(ActionEvent e) {
                text=textField1.getText();
                Scanner file=null;

                try {
                    file=new Scanner(new File(text));
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }

                while(file.hasNextLine()){
                    textArea1.append(file.nextLine());
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex3");
        frame.setContentPane(new ex3().Panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
