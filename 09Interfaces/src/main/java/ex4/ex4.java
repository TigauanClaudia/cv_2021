package ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ex4 {
    private JButton b11;
    private JButton b12;
    private JButton b13;
    private JButton b31;
    private JButton b32;
    private JButton b33;
    private JButton b21;
    private JButton b22;
    private JButton b23;
    private JPanel Panel;
    private JTextField textField1;
    private JButton b0;

    int n=0;
    public ex4() {
        b11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                n++;
                if(n%2==1)
                    b11.setText("X");
                else b11.setText("O");
                verifcastigator(b11.getText());

            }
        });
        b12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b12.setText("X");
                else b12.setText("O");
                verifcastigator(b12.getText());

            }
        });
        b13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b13.setText("X");
                else b13.setText("O");
                verifcastigator(b13.getText());

            }
        });
        b21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b21.setText("X");
                else b21.setText("O");
                verifcastigator(b21.getText());

            }
        });
        b22.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b22.setText("X");
                else b22.setText("O");
                verifcastigator(b22.getText());

            }
        });
        b23.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b23.setText("X");
                else b23.setText("O");
                verifcastigator(b23.getText());

            }
        });
        b31.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b31.setText("X");
                else b31.setText("O");
                verifcastigator(b31.getText());

            }
        });
        b32.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b32.setText("X");
                else b32.setText("O");
                verifcastigator(b32.getText());

            }
        });
        b33.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                if(n%2==1)
                    b33.setText("X");
                else b33.setText("O");
                verifcastigator(b33.getText());

            }
        });

        b0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b11.setText("");
                b12.setText("");
                b13.setText("");
                b21.setText("");
                b22.setText("");
                b23.setText("");
                b31.setText("");
                b32.setText("");
                b33.setText("");
                textField1.setText("");
                n=0;
            }
        });
    }

    void verifcastigator(String x) {
        if ((b11.getText() == x && b12.getText()==x && b13.getText()==x)
                || (b21.getText() == x && b22.getText()==x && b23.getText()==x)
                || (b31.getText() == x && b32.getText()==x && b33.getText()==x)
                || (b11.getText() == x && b21.getText()==x && b31.getText()==x)
                || (b12.getText() == x && b22.getText()==x && b32.getText()==x)
                || (b13.getText() == x && b23.getText()==x && b33.getText()==x)
                || (b11.getText() == x && b22.getText()==x && b33.getText()==x)
                || (b13.getText() == x && b22.getText()==x && b31.getText()==x)) textField1.setText("castigator: "+x);
        else if(n==9) textField1.setText("Remiza");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex4");
        frame.setContentPane(new ex4().Panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
