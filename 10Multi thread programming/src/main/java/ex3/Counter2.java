package ex3;

public class Counter2 extends Thread{

    Thread t;
    Counter2(String name, Thread t){
        super(name);
        this.t=t;
    }
    public void run(){
        for(int i=100;i<200;i++){
            if (t!=null) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }

}
