package ex4;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void initializeRobots(List<Robot> robotlist){
        for(int i=0;i<10;i++){
            robotlist.add(new Robot(i,i+1));
        }

    }
    public static void main(String[] args) {
        List<Robot> robotlist=new ArrayList<>(10);
        initializeRobots(robotlist);
        SetterThread setterThread=new SetterThread(robotlist);
        VerifThread verifThread=new VerifThread(robotlist);

        setterThread.start();
        verifThread.start();
    }
}
