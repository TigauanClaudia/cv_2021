package ex4;

import java.util.ArrayList;
import java.util.List;

public class VerifThread extends Thread{
    private final List<Robot> robots;

    public VerifThread(List<Robot> robots) {
        this.robots = robots;
    }

    @Override
    public void run() {
        while (!robots.isEmpty()) {
            synchronized (robots) {
                List<Robot> robotsRemoved = new ArrayList<>();
                for (int i = 0; i < robots.size(); i++) {
                    for (int j = i + 1; j < robots.size(); j++) {
                        if (robots.get(j).equals(robots.get(i))) {
                            robotsRemoved.add(robots.get(j));
                        }
                    }
                }

                System.out.println("Robots removed: " + robotsRemoved);
                robots.removeAll(robotsRemoved);

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
