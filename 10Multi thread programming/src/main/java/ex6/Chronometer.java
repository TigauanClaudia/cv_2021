package ex6;

public class Chronometer extends Thread {
    Interface i;

    public Chronometer(Interface i) {
        this.i = i;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            if (i.start) {
                i.n++;
                i.text.setText("n="+i.n + " ");
            } else {
                synchronized (i) {
                    try {
                        i.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
