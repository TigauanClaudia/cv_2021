package ex6;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame {

    JTextField text = new JTextField();
    JButton button1 = new JButton("Start/Stop");
    JButton button2 = new JButton("Reset");
    volatile boolean start = false;
    volatile int n;

    public Interface() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300, 60);
        this.setLayout(new GridLayout(1, 3));

        button1.addActionListener(e -> {
            synchronized (Interface.this) {
                notify();
                Interface.this.start = !Interface.this.start;
            }
        });

        button2.addActionListener(e -> {
            Interface.this.n = 0;
            text.setText(n + " ");
        });
        this.add(text);
        this.add(button1);
        this.add(button2);
        this.setVisible(true);
    }


}
