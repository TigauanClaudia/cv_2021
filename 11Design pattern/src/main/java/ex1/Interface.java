package ex1;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame implements Observer {
    JTextField text;

    @Override
    public void update(Object event) {
        text.setText(event.toString());
    }

    public Interface() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(200, 100);
        setLayout(new GridLayout(1, 2));
        setVisible(true);
        this.text = new JTextField("");
        this.add(text);

    }

}
