package ex1;

public class Main {
    public static void main(String[] args) {
        Sensor sensor = new Sensor();
        Interface i = new Interface();
        sensor.register(i);
        new Thread(sensor).start();
    }
}
