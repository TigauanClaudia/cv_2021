package ex1;

import java.util.Random;

public class Sensor extends Observable implements Runnable {
    Random r = new Random();

    @Override
    public void run() {
        while (true) {
            this.changeState(r.nextInt(125));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
