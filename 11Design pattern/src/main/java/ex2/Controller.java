package ex2;

import javax.swing.*;
import java.util.Vector;

public class Controller {
    private static final Vector<Product> products = new Vector<>();

    public static void main(String[] args) {
        JFrame frame = new JFrame("Interface");
        Interface i = new Interface();
        i.getAddNewProductButton().addActionListener(e -> {
            String productName = i.getTextField1().getText();
            int productQuantity = Integer.parseInt(i.getTextField2().getText());
            double productPrice = Double.parseDouble(i.getTextField3().getText());
            products.add(new Product(productName, productQuantity, productPrice));
        });
        i.getViewAvailableProductsButton().addActionListener(e -> {
            i.getList1().setListData(products);

        });
        i.getDeleteSelectedProductButton().addActionListener(e -> {
            products.remove(i.getList1().getSelectedValue());
            i.getList1().setListData(products);
        });

        i.getUpdateButton().addActionListener(e -> {
            products.get(i.getList1().getSelectedIndex()).setQuantity(Integer.parseInt(i.getTextField4().getText()));
        });
        frame.setContentPane(i.getEx2());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}