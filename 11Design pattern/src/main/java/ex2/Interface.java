package ex2;

import javax.swing.*;

public class Interface {
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton addNewProductButton;
    private JButton viewAvailableProductsButton;
    private JList list1;
    private JButton deleteSelectedProductButton;
    private JTextField textField4;
    private JButton updateButton;
    private JPanel ex2;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Interface");
        frame.setContentPane(new Interface().ex2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public void setTextField2(JTextField textField2) {
        this.textField2 = textField2;
    }

    public JTextField getTextField3() {
        return textField3;
    }

    public void setTextField3(JTextField textField3) {
        this.textField3 = textField3;
    }

    public JButton getAddNewProductButton() {
        return addNewProductButton;
    }

    public void setAddNewProductButton(JButton addNewProductButton) {
        this.addNewProductButton = addNewProductButton;
    }

    public JButton getViewAvailableProductsButton() {
        return viewAvailableProductsButton;
    }

    public void setViewAvailableProductsButton(JButton viewAvailableProductsButton) {
        this.viewAvailableProductsButton = viewAvailableProductsButton;
    }

    public JButton getDeleteSelectedProductButton() {
        return deleteSelectedProductButton;
    }

    public void setDeleteSelectedProductButton(JButton deleteSelectedProductButton) {
        this.deleteSelectedProductButton = deleteSelectedProductButton;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public void setUpdateButton(JButton updateButton) {
        this.updateButton = updateButton;
    }

    public void setList1(JList list1) {
        this.list1 = list1;
    }

    public JList getList1() {
        return list1;
    }

    public void setTextField4(JTextField textField4) {
        this.textField4 = textField4;
    }

    public JTextField getTextField4() {
        return textField4;
    }

    public JPanel getEx2() {
        return ex2;
    }

    public void setEx2(JPanel ex2) {
        this.ex2 = ex2;
    }
}
